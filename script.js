function calculate() {
    const n = parseInt(document.getElementById('n').value);
    if (isNaN(n) || n < 0) {
        alert("n must be bigger than 0!");
        return;
    }

    const startRecursive = performance.now();
    const recursiveResult = recursiveProduct(n);
    const endRecursive = performance.now();

    const startIterative = performance.now();
    const iterativeResult = iterativeProduct(n);
    const endIterative = performance.now();

    document.getElementById('results').innerHTML = `
                <p>Recursion result: ${recursiveResult}</p>
                <p>Recursion time: ${(endRecursive - startRecursive).toFixed(4)} ms</p>
                <p>Iteration result: ${iterativeResult}</p>
                <p>Iteration time: ${(endIterative - startIterative).toFixed(4)} ms</p>
            `;
}

function recursiveProduct(n) {
    if (n === 0) {
        return 1 ;
    }
    return (1 / (n + 1)) * recursiveProduct(n - 1);
}

function iterativeProduct(n) {
    let result = 1
    for (let i = 1; i <= n; i++) {
        result *= (1 / (i + 1));
    }
    return result;
}